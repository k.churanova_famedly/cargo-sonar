# Changelog
All notable changes to this project will be documented in this file. See [conventional commits](https://www.conventionalcommits.org/) for commit guidelines.

- - -
## [0.12.0](https://gitlab.com/woshilapin/cargo-sonar/-/compare/0.11.0..0.12.0) - 2022-07-19
#### Bug Fixes
- remove a 'clippy::pattern_type_mismatch' - ([ab0b226](https://gitlab.com/woshilapin/cargo-sonar/-/commit/ab0b226863179c45b22f3f758e35694ff2a9835c)) - Jean SIMARD
#### Continuous Integration
- update to 'cargo-deny:0.11.2' - ([5b89b69](https://gitlab.com/woshilapin/cargo-sonar/-/commit/5b89b696f9b913692e429324c4196764410be430)) - Jean SIMARD
- update to 'typos-cli:1.4.0' - ([6664a57](https://gitlab.com/woshilapin/cargo-sonar/-/commit/6664a5796c5c2b8b838af059c008f4703501f408)) - Jean SIMARD
#### Features
- upgrade all dependencies - ([7efd5ae](https://gitlab.com/woshilapin/cargo-sonar/-/commit/7efd5ae2fd2a672b2ce9ce80f35759fdb824b995)) - Jean SIMARD
- upgrade to 'cocogitto:5.1.0' - ([1b5996b](https://gitlab.com/woshilapin/cargo-sonar/-/commit/1b5996b9cd19ab12e48106194278a75590be7796)) - Jean SIMARD
- upgrade to 'cargo-outdated:0.11.1' - ([7571f15](https://gitlab.com/woshilapin/cargo-sonar/-/commit/7571f155928692cac3264503d6db30d28d09339e)) - Jean SIMARD
- upgrade to 'cargo-deny:0.12.1' - ([23b97ac](https://gitlab.com/woshilapin/cargo-sonar/-/commit/23b97ac33e80af3f6c673c978b71a255c7205d09)) - Jean SIMARD
- upgrade to 'cargo-audit:0.17.0' - ([aad5e56](https://gitlab.com/woshilapin/cargo-sonar/-/commit/aad5e566258a8b8e0c68c22ab16c8f56767adb26)) - Jean SIMARD
- upgrade to 'tarpaulin:0.20.1' - ([6919993](https://gitlab.com/woshilapin/cargo-sonar/-/commit/6919993449ae01008eae79f8b1bc248c248d508b)) - Jean SIMARD
- upgrade to 'typos:1.10.2' - ([09dfd51](https://gitlab.com/woshilapin/cargo-sonar/-/commit/09dfd51b31ee9e9d2fe708f39ae8edd6caeba2dd)) - Jean SIMARD
- upgrade to 'cargo-udeps:0.1.30' - ([5191824](https://gitlab.com/woshilapin/cargo-sonar/-/commit/51918243420b83c2ce142bc0a45639777590a791)) - Jean SIMARD

- - -

## [0.11.0](https://gitlab.com/woshilapin/cargo-sonar/-/compare/0.10.2..0.11.0) - 2022-02-04
#### Bug Fixes
- calling 'cargo sonar' does not fail anymore - ([9b57001](https://gitlab.com/woshilapin/cargo-sonar/-/commit/9b570016bbd5f9d79ad3190191be0c9b25b71055)) - Jean SIMARD
#### Features
- update all dependencies - ([9e6fbfb](https://gitlab.com/woshilapin/cargo-sonar/-/commit/9e6fbfb50b432d77067f0dbcfc87ce3154065c7d)) - Jean SIMARD
- - -

## [0.10.2](https://gitlab.com/woshilapin/cargo-sonar/-/compare/0.10.1..0.10.2) - 2022-02-04
#### Bug Fixes
- allow running without having --coverage specified - ([4779432](https://gitlab.com/woshilapin/cargo-sonar/-/commit/4779432697bff7432420f197bdad330a54484203)) - Kristof Mattei
#### Continuous Integration
- update to 'cargo-deny:0.11.1' - ([5887816](https://gitlab.com/woshilapin/cargo-sonar/-/commit/5887816360e63c6c117a38bbce77334a5e95e07d)) - Jean SIMARD
- update to 'typos-cli:1.3.9' - ([852f519](https://gitlab.com/woshilapin/cargo-sonar/-/commit/852f5198775316e717a5b4ebd63fe5c0e681b142)) - Jean SIMARD
- update to 'typos-cli:1.3.7' - ([2845d6d](https://gitlab.com/woshilapin/cargo-sonar/-/commit/2845d6d7199db495ec894b1a8f23815655ea69bd)) - Jean SIMARD
- update to 'typos-cli:1.3.6' - ([fac1fdc](https://gitlab.com/woshilapin/cargo-sonar/-/commit/fac1fdc00fb1d34614d69455976585f695e61d81)) - Jean SIMARD
- update to 'cargo-udeps:0.1.26' - ([8788752](https://gitlab.com/woshilapin/cargo-sonar/-/commit/8788752d63ff084100523549e6570c0cb97e914f)) - Jean SIMARD
- update to 'tarpaulin:0.19.1' - ([6617014](https://gitlab.com/woshilapin/cargo-sonar/-/commit/66170148e9d0d48bd01e8d5d4220e2e4977f0b33)) - Jean SIMARD
- update to 'cargo-audit:0.16.0' - ([ec4a347](https://gitlab.com/woshilapin/cargo-sonar/-/commit/ec4a3479eac7ac24daea3a542c4e647c3766700f)) - Jean SIMARD
- update to 'cocogitto:4.1.0' - ([703f9b4](https://gitlab.com/woshilapin/cargo-sonar/-/commit/703f9b4f8fee3865622b7bc4d7b887ad66ab1891)) - Jean SIMARD
- update to 'typos-cli:1.3.4' - ([78cc114](https://gitlab.com/woshilapin/cargo-sonar/-/commit/78cc1149193682f5ea3dfdd4613638df95a45b58)) - Jean SIMARD
- do not run 'sonar' when 'SONAR_TOKEN' isn't available - ([b1567a5](https://gitlab.com/woshilapin/cargo-sonar/-/commit/b1567a5af2774ef7448575fe0091f2de9e1607d2)) - Jean SIMARD
- - -

## [0.10.1](https://gitlab.com/woshilapin/cargo-sonar/-/compare/0.10.0..0.10.1) - 2021-12-15
#### Bug Fixes
- **(outdated)** handle multi-workspace projects - ([6f4a7eb](https://gitlab.com/woshilapin/cargo-sonar/-/commit/6f4a7eb8bf425e0ecf8adc00afcc11a0dbfd2ca0)) - Thomas Loubiou
- - -

## [0.10.0](https://gitlab.com/woshilapin/cargo-sonar/-/compare/0.9.0..0.10.0) - 2021-12-15
#### Bug Fixes
- make 'clippy' much more strict and in-code - ([661e629](https://gitlab.com/woshilapin/cargo-sonar/-/commit/661e629e4be35903cf8bba81f5a16e2e48a9c7eb)) - Jean SIMARD
- remove some clippy warnings for enum and dead_code - ([6a1e2c7](https://gitlab.com/woshilapin/cargo-sonar/-/commit/6a1e2c7b29db4a33569d0924267cdd14552918ce)) - Jean SIMARD
#### Continuous Integration
- deactivate coverage publishing to sonarcloud - ([6d31821](https://gitlab.com/woshilapin/cargo-sonar/-/commit/6d31821de53e3286b89396815c1d1c019a94b2bb)) - Jean SIMARD
- update to 'typos-cli:1.3.2' - ([b885a26](https://gitlab.com/woshilapin/cargo-sonar/-/commit/b885a26bd9a17490586e74037063fea99d93ba75)) - Jean SIMARD
- define the default CI image correctly - ([860ed6b](https://gitlab.com/woshilapin/cargo-sonar/-/commit/860ed6bf44b86f18f4616cbd7e7dede6eee189bb)) - Jean SIMARD
- add 'interruptible' jobs to not waste CI time - ([c0c1cb4](https://gitlab.com/woshilapin/cargo-sonar/-/commit/c0c1cb484f289ef03a5a369a75ffa7ec2b75c695)) - Jean SIMARD
- update to 'cargo-udeps:0.25.0' - ([3c9ff76](https://gitlab.com/woshilapin/cargo-sonar/-/commit/3c9ff7694e22df53b30afb27e519e51b3901adc8)) - Jean SIMARD
- update to 'cargo-deny:0.11.0' - ([b4980c1](https://gitlab.com/woshilapin/cargo-sonar/-/commit/b4980c1a3377228291ec971fcf0ec266ec375a25)) - Jean SIMARD
- update to 'cocogitto:4.0.1' - ([ef29db7](https://gitlab.com/woshilapin/cargo-sonar/-/commit/ef29db7d918eeed0868277c3aa46c1f459b1f0e1)) - Jean SIMARD
#### Features
- add coverage conversion for 'cargo-tarpaulin' - ([abf3b11](https://gitlab.com/woshilapin/cargo-sonar/-/commit/abf3b11c95dd4a270cf2049e28092f7f809ebde3)) - Jean SIMARD
- - -

## 0.9.0 - 2021-11-29


### Documentation

[0531a8](https://gitlab.com/woshilapin/cargo-sonar/-/commit/0531a8b4734a3bc70a9f9a0047e1aeabad43e6f8) - remove useless comment (already fixed) - Jean SIMARD

[02741b](https://gitlab.com/woshilapin/cargo-sonar/-/commit/02741b995447cd7437f297b189ef20918c72095c) - fix a typo in a FIXME - Jean SIMARD


### Features

[c995e1](https://gitlab.com/woshilapin/cargo-sonar/-/commit/c995e1c67f59200245e34656faac422f440d8cef) - change CLI activation of options - Jean SIMARD

[1b2e78](https://gitlab.com/woshilapin/cargo-sonar/-/commit/1b2e78fd2092fd108da393888cd788745d6983e0) - support for 'cargo-udeps' - Jean SIMARD

[af61ac](https://gitlab.com/woshilapin/cargo-sonar/-/commit/af61acbf2edcefbb349ec47ce391774be6876d1b) - update all dependencies - Jean SIMARD

[b19397](https://gitlab.com/woshilapin/cargo-sonar/-/commit/b1939780449415f0351ae83624d4ae1c8178b9cc) - update 'tracing-subscriber' dependency - Jean SIMARD

[901906](https://gitlab.com/woshilapin/cargo-sonar/-/commit/90190694c1a37ff885058ea415af760c0873e4d4) - update 'thiserror' dependency - Jean SIMARD

[ab7d78](https://gitlab.com/woshilapin/cargo-sonar/-/commit/ab7d7815bc5ee7f2c122934bdfc6018922a4aa19) - update 'serde_json' dependency - Jean SIMARD

[fdbeb1](https://gitlab.com/woshilapin/cargo-sonar/-/commit/fdbeb18fdd708d430713353178bb6b2eb440eb52) - update 'rustsec' dependency - Jean SIMARD


### Continuous Integration

[18ccf6](https://gitlab.com/woshilapin/cargo-sonar/-/commit/18ccf68354948d477e15454989723b495fbaebd2) - rely more on dependencies to speed up the CI - Jean SIMARD

[60d538](https://gitlab.com/woshilapin/cargo-sonar/-/commit/60d538938eab1ef007cf382e77d0f7690fd9f555) - update 'cargo-deny' dependency - Jean SIMARD


### Bug Fixes

[4770e6](https://gitlab.com/woshilapin/cargo-sonar/-/commit/4770e6a4ef6cf0627441a7dd362cbefaf2ff0fae) - remove most clippy warnings - Jean SIMARD

[f1d7c0](https://gitlab.com/woshilapin/cargo-sonar/-/commit/f1d7c0cb445a8b91f24f28280b29968cba504445) - remove unused dependency - Jean SIMARD

[b9f049](https://gitlab.com/woshilapin/cargo-sonar/-/commit/b9f049f97cb47eb71c28cc3b2e357be58557935a) - improve feature dependencies - Jean SIMARD

[9e2b67](https://gitlab.com/woshilapin/cargo-sonar/-/commit/9e2b6747f5c1fe4b00fb4ab41c85d5baee717d98) - 'cargo-outdated' is not only-JSON when success - Jean SIMARD


### Miscellaneous Chores

[aad8f5](https://gitlab.com/woshilapin/cargo-sonar/-/commit/aad8f5d9ff707261cb254d892d3c799bde706593) - remove all json report from git - Jean SIMARD


- - -
## 0.8.1 - 2021-11-18


### Bug Fixes

[c1ca82](https://gitlab.com/woshilapin/cargo-sonar/-/commit/c1ca82b7c5a0afa0a0a6cf03f237e50b6051e5b5) - do not fail because of end-of-line characters - Jean SIMARD


### Documentation

[38ad11](https://gitlab.com/woshilapin/cargo-sonar/-/commit/38ad11d88ad0623d949f871f6a4ccf96e263cf75) - improve documentation of 'cargo-deny' - Jean SIMARD

[97518f](https://gitlab.com/woshilapin/cargo-sonar/-/commit/97518f1d642d56d770e5577cfacda62202e9837f) - improve documentation of 'cargo-outdated' - Jean SIMARD


- - -
## 0.8.0 - 2021-11-17


### Refactoring

[7b7b92](https://gitlab.com/woshilapin/cargo-sonar/-/commit/7b7b9277348ec792524d24e4792f5a0ecf708ac5) - change lifetimes' names - Jean SIMARD


### Documentation

[553dde](https://gitlab.com/woshilapin/cargo-sonar/-/commit/553dde03c253e722ece07603c2db898507a68f31) - update TODO list in the README.md - Jean SIMARD


### Bug Fixes

[272374](https://gitlab.com/woshilapin/cargo-sonar/-/commit/2723740814354100decd1d1ffe239f56db80b7b3) - improve 'rule_id' to make them more unique - Jean SIMARD


### Features

[cae45f](https://gitlab.com/woshilapin/cargo-sonar/-/commit/cae45f8293bc68e35b699b6d7b1663fdc310dd98) - add support for 'cargo-outdated' - Jean SIMARD


### Continuous Integration

[91d3d6](https://gitlab.com/woshilapin/cargo-sonar/-/commit/91d3d61be8358906e835840e11569299fae580b8) - add 'cargo-outdated' to the CI - Jean SIMARD


- - -
## 0.7.0 - 2021-11-17


### Features

[1c2755](https://gitlab.com/woshilapin/cargo-sonar/-/commit/1c2755b4ee05b5450647dbc5f860d46e8ae3265e) - support for 'cargo-deny' - Jean SIMARD


### Continuous Integration

[868036](https://gitlab.com/woshilapin/cargo-sonar/-/commit/868036cc39eea83bac8fc465124fb85cb7f1dae8) - update to 'typos:1.3.1' - Jean SIMARD

[d44915](https://gitlab.com/woshilapin/cargo-sonar/-/commit/d44915cb8f9b58d2083c979545de66c639c76136) - add 'cargo-deny' to the Sonar report - Jean SIMARD

[ae6651](https://gitlab.com/woshilapin/cargo-sonar/-/commit/ae665139443b6880b84d0e12c7cf7929efe44e05) - upgrade to 'typos:1.3.0' - Jean SIMARD


- - -
## 0.6.0 - 2021-11-11


### Continuous Integration

[14f44c](https://gitlab.com/woshilapin/cargo-sonar/-/commit/14f44cc674e1112c9c78a7a9e70f67d63199d42c) - fix the extraction of 'cargo-tomlfmt' - Jean SIMARD

[53b4cb](https://gitlab.com/woshilapin/cargo-sonar/-/commit/53b4cbceed16adf5c7aa831f1031c7e337b364be) - do not exclude 'Cargo.lock' file, new version of 'typos' does it by default - Jean SIMARD

[f56859](https://gitlab.com/woshilapin/cargo-sonar/-/commit/f56859779d854216d8183672d69dc913f223f550) - fix the stage for sonar reporting - Jean SIMARD

[c4c319](https://gitlab.com/woshilapin/cargo-sonar/-/commit/c4c319939cbd2b5a94e17db6e08e1377a552a7f7) - add 'cargo-tomlfmt' in the CI check - Jean SIMARD


### Documentation

[8e206b](https://gitlab.com/woshilapin/cargo-sonar/-/commit/8e206b95fd0c5b2d36025b3e75e2f66ace43ef7f) - how to release manually - Jean SIMARD


### Features

[50e5bb](https://gitlab.com/woshilapin/cargo-sonar/-/commit/50e5bb9583d2f76047919efc8091eea6c64c330d) - use 'Cargo.lock' instead of 'Cargo.toml' for 'cargo-audit' - Jean SIMARD


### Refactoring

[98577d](https://gitlab.com/woshilapin/cargo-sonar/-/commit/98577d5b65a000cd54083b98178c9f055dd2ffb2) - rename the 'Cargo.toml' parsing into 'Manifest' - Jean SIMARD


- - -
## 0.5.1 - 2021-11-10


### Bug Fixes

[26bfa7](https://gitlab.com/woshilapin/cargo-sonar/-/commit/26bfa76929e68d5be8f3d3fb97fe8689cecf708a) - add useful error message when input report is missing - Jean SIMARD


### Continuous Integration

[86fbea](https://gitlab.com/woshilapin/cargo-sonar/-/commit/86fbea7dbaea2b9b2894698818378a6402bc515b) - remove the caching mechanism and use artifacts - Jean SIMARD

[c4a5bc](https://gitlab.com/woshilapin/cargo-sonar/-/commit/c4a5bc43de58526673c68f702653b0a9c6c0fbb2) - fix caches - Jean SIMARD


- - -
## 0.5.0 - 2021-11-10


### Features

[52da0e](https://gitlab.com/woshilapin/cargo-sonar/-/commit/52da0e10fc6de13eb08ac531ce48ba48276884d1) - make the output file configurable - Jean SIMARD


### Continuous Integration

[349e1f](https://gitlab.com/woshilapin/cargo-sonar/-/commit/349e1f346f56699bf2d4f30587c0cfb2a4f386e0) - fix the caching mechanism - Jean SIMARD


- - -
## 0.4.0 - 2021-11-10


### Refactoring

[eb04ed](https://gitlab.com/woshilapin/cargo-sonar/-/commit/eb04edf45d81562db230b449e38ec3d7b56da77f) - rename some 'sonar' variables - Jean SIMARD


### Continuous Integration

[ca0caa](https://gitlab.com/woshilapin/cargo-sonar/-/commit/ca0caa0cd5a1c74204ead6cf3939cf01ff745a8b) - 'git clean' before 'cog bump' - Jean SIMARD

[62384e](https://gitlab.com/woshilapin/cargo-sonar/-/commit/62384e18ec636be888a5b8e271f7128ea48494dc) - add some documentation and new stages - Jean SIMARD

[2f127b](https://gitlab.com/woshilapin/cargo-sonar/-/commit/2f127b54575661fb50761b7f922c6b119449ced7) - sonar scanner - Jean SIMARD

[98a164](https://gitlab.com/woshilapin/cargo-sonar/-/commit/98a164b2c019f59727ce0c25d2177e608bbd7f72) - fix check on conventional commit - Jean SIMARD

[04c2c3](https://gitlab.com/woshilapin/cargo-sonar/-/commit/04c2c3917fcf8e6d600003b74334950c8a33d8ad) - reorder steps in alphabetical order with comments - Jean SIMARD

[734490](https://gitlab.com/woshilapin/cargo-sonar/-/commit/7344902d010b2d4b93227ed455caa3f10594f773) - add cargo-audit in CI - Jean SIMARD

[d2e6ac](https://gitlab.com/woshilapin/cargo-sonar/-/commit/d2e6ac63fa33f2845a1da9a65a15f4689b8774f7) - add cache for clippy.json - Jean SIMARD


### Documentation

[5fdfb9](https://gitlab.com/woshilapin/cargo-sonar/-/commit/5fdfb97a3dae8d9db57f89a3dececfdd368785fa) - add serious documentation in README - Jean SIMARD


### Build system

[98f539](https://gitlab.com/woshilapin/cargo-sonar/-/commit/98f5398277ea853e1c477ed97f7e846c97072485) - enable incremental compilation to use 'cargo-chef' - Jean SIMARD


### Features

[76374a](https://gitlab.com/woshilapin/cargo-sonar/-/commit/76374a5873e7300a017b20e940e80a67655e054b) - add CLI configuration - Jean SIMARD

[d9a2ff](https://gitlab.com/woshilapin/cargo-sonar/-/commit/d9a2ffae9bf0e4984aa7dc47cf19f15e4d37fc37) - activate all features by default - Jean SIMARD

[12e58a](https://gitlab.com/woshilapin/cargo-sonar/-/commit/12e58a6d6afdd4ee689c244aadc8362f10c50aa6) - put 'audit' and 'clippy' functionalities behind features - Jean SIMARD

[ffe844](https://gitlab.com/woshilapin/cargo-sonar/-/commit/ffe84499c25f23c1dd4dadb96f8d06705ab4aa24) - update all dependencies - Jean SIMARD


- - -
## 0.3.1 - 2021-11-04


### Bug Fixes

[fe2eb4](https://gitlab.com/woshilapin/cargo-sonar/-/commit/fe2eb4d07418bcbe61040c4cd356b0967f2f801d) - binary access in the container image - Jean SIMARD


- - -
## 0.3.0 - 2021-11-03


### Miscellaneous Chores

[7650a2](https://gitlab.com/woshilapin/cargo-sonar/-/commit/7650a2f68f40bf9bfd19295c8950390be30fe254) - add 'rust-toolchain.toml' - Jean SIMARD


### Build system

[651e7a](https://gitlab.com/woshilapin/cargo-sonar/-/commit/651e7affb2ecdf487906b72382839da590141bf8) - remove link optimization that causes problems everywhere - Jean SIMARD

[e6773d](https://gitlab.com/woshilapin/cargo-sonar/-/commit/e6773d606cce946d1583abba5b3a7b1075808c84) - improve local build's time - Jean SIMARD


### Features

[277f68](https://gitlab.com/woshilapin/cargo-sonar/-/commit/277f68d51453ab5a0161de0dc4078f5f0accbefa) - do not depend on binaries anymore - Jean SIMARD


- - -
## 0.2.0 - 2021-10-27


### Miscellaneous Chores

[8fe82b](https://gitlab.com/woshilapin/cargo-sonar/-/commit/8fe82b90549eae847114f3838cf2a0fedeb132fa) - add some FIXME/TODO with ideas for improvements - Jean SIMARD


### Features

[44412c](https://gitlab.com/woshilapin/cargo-sonar/-/commit/44412c8ee403fa02e7c1d597528e12cc96075fb1) - clippy is configured in 'pedantic' mode - Jean SIMARD

[61ef39](https://gitlab.com/woshilapin/cargo-sonar/-/commit/61ef39b52466a172c2c66567d482f3081419d9d2) - docker image now includes 'clippy' and 'audit' - Jean SIMARD


- - -
## 0.1.3 - 2021-10-26


### Continuous Integration

[1e04f7](https://gitlab.com/woshilapin/cargo-sonar/-/commit/1e04f7d943f59d6d70a89938e78188764c9eea3a) - update 'Cargo.lock' when tagging a new version - Jean SIMARD


### Bug Fixes

[77793c](https://gitlab.com/woshilapin/cargo-sonar/-/commit/77793c7392fdd9d674f51e8659b01849eb574078) - remove full-path of trait in the edition2021's prelude - Jean SIMARD


- - -
## 0.1.2 - 2021-10-26


### Tests

[c37008](https://gitlab.com/woshilapin/cargo-sonar/-/commit/c37008edfb3fb9803dc75d0273976f18b024b43a) - add test for cargo parsing - Jean SIMARD


### Bug Fixes

[61b46b](https://gitlab.com/woshilapin/cargo-sonar/-/commit/61b46b63c36bc1a5a385219ef1d79b6b2e54832f) - TryFrom/TryInto is in the prelude in edition2021 - Jean SIMARD


### Continuous Integration

[b316e7](https://gitlab.com/woshilapin/cargo-sonar/-/commit/b316e7e538aad2dddb393843e954b7e07fc8ca5b) - add 'cargo test' - Jean SIMARD

[8f6924](https://gitlab.com/woshilapin/cargo-sonar/-/commit/8f692444d1f9f8d1d1238b7cd1200de9a05e4639) - remove useless braces for variables - Jean SIMARD

[5137b5](https://gitlab.com/woshilapin/cargo-sonar/-/commit/5137b5e9a8a1759a618825ff597d1389283603d3) - use full-path container image - Jean SIMARD


- - -
## 0.1.1 - 2021-10-25


### Continuous Integration

[1e71e9](https://gitlab.com/woshilapin/cargo-sonar/-/commit/1e71e9ba3b4465bc53f5dbe824df6b3bb106ea6c) - automatically bump version of Cargo.toml - Jean SIMARD

[effd19](https://gitlab.com/woshilapin/cargo-sonar/-/commit/effd19bf84b8be1b0427a880c5a987a0089e2f38) - setting up git user is only useful for tagging - Jean SIMARD

[4c9fd1](https://gitlab.com/woshilapin/cargo-sonar/-/commit/4c9fd19c1be0ec60fee208788d572a39a033bdb7) - automatic release of OCI image to Dockerhub - Jean SIMARD


### Miscellaneous Chores

[abef2d](https://gitlab.com/woshilapin/cargo-sonar/-/commit/abef2d5e67f037990798af76bdf89fb4b7340867) - revert 0.1.1 - Jean SIMARD

[258609](https://gitlab.com/woshilapin/cargo-sonar/-/commit/25860929833ef4315850443aa1c492898f3ac11e) - 0.1.1 - gitlab-ci


### Bug Fixes

[9df357](https://gitlab.com/woshilapin/cargo-sonar/-/commit/9df3575cd71aa4f78b02c0edd6bb995627c22c42) - need to update to 'cargo:0.57' to support edition 2021 - Jean SIMARD


### Documentation

[845c08](https://gitlab.com/woshilapin/cargo-sonar/-/commit/845c08ea330ac7183a056b7df3538b4478a0d5ad) - add TODO about documentation - Jean SIMARD

[7fc945](https://gitlab.com/woshilapin/cargo-sonar/-/commit/7fc945e5baac6b5cd03beec7a088b6e2c2bf5d33) - open-source the project - Jean SIMARD


### Build system

[b1163c](https://gitlab.com/woshilapin/cargo-sonar/-/commit/b1163c6bddfac794070790358e5af67227e964c6) - update to Rust 1.56.0 - Jean SIMARD


- - -
## 0.1.0 - 2021-10-25


### Documentation

[ffa03c](https://gitlab.com/woshilapin/cargo-sonar/-/commit/ffa03cf9f907b90d43f0f95f24f9c9334e9e9327) - add a license file - Jean SIMARD

[4028b0](https://gitlab.com/woshilapin/cargo-sonar/-/commit/4028b00ea50925d45c6e7aae8345f4a711313254) - add a 'README.md' - Jean SIMARD


### Continuous Integration

[1b841b](https://gitlab.com/woshilapin/cargo-sonar/-/commit/1b841becd711ba2209f1fd18ef5579d24d1eb1c6) - automatic release - Jean SIMARD

[ef25d4](https://gitlab.com/woshilapin/cargo-sonar/-/commit/ef25d47c61e76144c74356c7ae24fe5874795c25) - add typos checking - Jean SIMARD

[27f42b](https://gitlab.com/woshilapin/cargo-sonar/-/commit/27f42b2f8c4c0c6fe8f5798a655eacbf5980c301) - improve conventional commit check - Jean SIMARD

[d58a17](https://gitlab.com/woshilapin/cargo-sonar/-/commit/d58a177135a31f906a3ade7249686101ca4c18a6) - add sonar-scanner configuration - Jean SIMARD

[6ae7e1](https://gitlab.com/woshilapin/cargo-sonar/-/commit/6ae7e1361896fb60214dedd07f2c64edda54d811) - add cocogitto check for conventional commits - Jean SIMARD

[004a35](https://gitlab.com/woshilapin/cargo-sonar/-/commit/004a35ab4b50cbf992db0b24f835e1f835e74907) - explode in multiple files - Jean SIMARD

[b4fac6](https://gitlab.com/woshilapin/cargo-sonar/-/commit/b4fac680ac29066bf0a8a907b5c5fb905c51c0df) - add clippy check - Jean SIMARD


### Features

[ebb1b4](https://gitlab.com/woshilapin/cargo-sonar/-/commit/ebb1b47c8fa67f23e8c533d50383b4f7d68ed8da) - convert audit reports into sonar reports - Jean SIMARD

[3f86d4](https://gitlab.com/woshilapin/cargo-sonar/-/commit/3f86d4f0fd66e90b2a5c7e75d00b7f293b51e724) - add cargo parsing for text ranges - Jean SIMARD

[a8293e](https://gitlab.com/woshilapin/cargo-sonar/-/commit/a8293eeb56849bac32a112f5395f25f8fdcd0b98) - replace 'anyhow' with 'eyre' and 'color_eyre' - Jean SIMARD

[10aeb1](https://gitlab.com/woshilapin/cargo-sonar/-/commit/10aeb1a871a7cf98d98fe9164a5bc2bf1d340b6b) - add error handling with anyhow - Jean SIMARD

[5ada39](https://gitlab.com/woshilapin/cargo-sonar/-/commit/5ada39ec7a74ca663fb243824e2ab58fc6b59311) - add 'tracing' for logging - Jean SIMARD

[721099](https://gitlab.com/woshilapin/cargo-sonar/-/commit/7210997e23e43e2b5e88d5e7736cb93d1c3d7085) - run 'cargo audit' without parsing yet - Jean SIMARD

[7d0bb9](https://gitlab.com/woshilapin/cargo-sonar/-/commit/7d0bb95a1fe107c6fbdfe137d67a6212eff87296) - stream cargo clippy result to produce output - Jean SIMARD


### Miscellaneous Chores

[3d71bc](https://gitlab.com/woshilapin/cargo-sonar/-/commit/3d71bc5f5a360dae836190b7b1630312ed62bb1a) - improve metadata in Cargo.toml - Jean SIMARD


### Refactoring

[5add33](https://gitlab.com/woshilapin/cargo-sonar/-/commit/5add330e9c715fc4164a22fbc9143241b3643656) - move all parsing into sub-modules - Jean SIMARD


- - -

This changelog was generated by [cocogitto](https://github.com/oknozor/cocogitto).