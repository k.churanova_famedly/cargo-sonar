#![deny(clippy::all, clippy::cargo)]
// Clippy restrictions
#![warn(
    clippy::pedantic,
    clippy::as_conversions,
    clippy::create_dir,
    clippy::dbg_macro,
    clippy::decimal_literal_representation,
    clippy::exhaustive_enums,
    clippy::expect_used,
    clippy::filetype_is_file,
    clippy::float_cmp_const,
    clippy::get_unwrap,
    clippy::if_then_some_else_none,
    clippy::indexing_slicing,
    clippy::integer_arithmetic,
    clippy::integer_division,
    clippy::let_underscore_must_use,
    clippy::lossy_float_literal,
    clippy::map_err_ignore,
    clippy::mem_forget,
    clippy::missing_inline_in_public_items,
    clippy::mod_module_files,
    clippy::multiple_inherent_impl,
    clippy::panic,
    clippy::panic_in_result_fn,
    clippy::pattern_type_mismatch,
    clippy::rest_pat_in_fully_bound_structs,
    clippy::same_name_method,
    clippy::unseparated_literal_suffix,
    clippy::str_to_string,
    clippy::string_add,
    clippy::string_to_string,
    clippy::todo,
    clippy::unimplemented,
    clippy::unnecessary_self_imports,
    clippy::unneeded_field_pattern,
    clippy::unwrap_in_result,
    clippy::unwrap_used,
    clippy::use_debug,
    clippy::verbose_file_reads
)]
#![doc = include_str!("../README.md")]

#[cfg(feature = "audit")]
mod audit;
#[cfg(any(
    feature = "audit",
    feature = "deny",
    feature = "outdated",
    feature = "udeps"
))]
mod cargo;
mod cli;
#[cfg(feature = "clippy")]
mod clippy;
#[cfg(feature = "deny")]
mod deny;
#[cfg(feature = "outdated")]
mod outdated;
mod sonar;
#[cfg(feature = "tarpaulin")]
mod tarpaulin;
#[cfg(feature = "udeps")]
mod udeps;

use clap::Parser as _;
use eyre::{Context as _, Result};
use serde::Serialize;
use std::fs::File;
use tracing::{info, Level};
use tracing_subscriber::{prelude::*, EnvFilter};

fn init_tracer() {
    let default_level = Level::INFO;
    let rust_log =
        std::env::var(EnvFilter::DEFAULT_ENV).unwrap_or_else(|_| default_level.to_string());
    let env_filter_subscriber = EnvFilter::try_new(rust_log).unwrap_or_else(|e| {
        eprintln!(
            "invalid {}, falling back to level '{}' - {}",
            EnvFilter::DEFAULT_ENV,
            default_level,
            e,
        );
        EnvFilter::new(default_level.to_string())
    });
    tracing_subscriber::registry()
        .with(tracing_subscriber::fmt::layer())
        .with(env_filter_subscriber)
        .init();
}

fn main() -> Result<()> {
    init_tracer();
    color_eyre::install()?;
    // When calling `cargo sonar` binary, the first 2 arguments are
    // - `cargo-sonar`
    // - `sonar`
    // which is different from calling the binary directly.
    // If `sonar` is the second argument, we filter it out.
    let options = cli::Command::parse_from(
        std::env::args_os()
            .enumerate()
            .filter(|&(ref i, ref args)| !(*i == 1 && args == "sonar"))
            .map(|(_, args)| args),
    );

    #[cfg(any(
        feature = "audit",
        feature = "clippy",
        feature = "deny",
        feature = "outdated",
        feature = "udeps"
    ))]
    generate_issues(&options)?;

    #[cfg(any(feature = "tarpaulin"))]
    generate_coverage(&options)?;

    Ok(())
}

fn generate_issues(options: &cli::Command) -> Result<(), color_eyre::Report> {
    #[cfg(any(
        feature = "audit",
        feature = "deny",
        feature = "outdated",
        feature = "udeps"
    ))]
    let lockfile = {
        use std::env::current_dir;
        let lockfile_path = current_dir()?.join("Cargo.lock");
        cargo::Lockfile::try_from(lockfile_path.as_path())?
    };
    let mut issues_report = sonar::Issues::default();
    for issue in &options.issues {
        match *issue {
            #[cfg(feature = "audit")]
            cli::Issue::Audit => {
                use crate::audit::Audit;
                let audit_report: sonar::Issues =
                    Audit::new(&options.audit_path, &lockfile).try_into()?;
                issues_report.extend(audit_report);
            }
            #[cfg(feature = "clippy")]
            cli::Issue::Clippy => {
                use crate::clippy::Clippy;
                let clippy_report: sonar::Issues = Clippy::new(&options.clippy_path).try_into()?;
                issues_report.extend(clippy_report);
            }
            #[cfg(feature = "deny")]
            cli::Issue::Deny => {
                use crate::deny::Deny;
                let deny_report: sonar::Issues =
                    Deny::new(&options.deny_path, &lockfile).try_into()?;
                issues_report.extend(deny_report);
            }
            #[cfg(feature = "outdated")]
            cli::Issue::Outdated => {
                use crate::outdated::Outdated;
                let outdated_report: sonar::Issues =
                    Outdated::new(&options.outdated_path, &lockfile).try_into()?;
                issues_report.extend(outdated_report);
            }
            #[cfg(feature = "udeps")]
            cli::Issue::Udeps => {
                use crate::udeps::Udeps;
                let udeps_report: sonar::Issues =
                    Udeps::new(&options.udeps_path, &lockfile).try_into()?;
                issues_report.extend(udeps_report);
            }
        }
    }
    let file = File::create(&options.issues_path).context(format!(
        "failed to create '{:?}' file",
        &options.issues_path
    ))?;
    info!("{} sonar issues created", issues_report.len());
    #[cfg(not(debug_assertions))]
    let writer = serde_json::to_writer;
    #[cfg(debug_assertions)]
    let writer = serde_json::to_writer_pretty;
    writer(file, &issues_report).context(format!(
        "failed to write sonar issues to '{:?}' file",
        &options.issues_path,
    ))?;
    Ok(())
}

fn generate_coverage(options: &cli::Command) -> Result<(), color_eyre::Report> {
    let coverage_report = match options.coverage {
        #[cfg(feature = "tarpaulin")]
        Some(cli::Coverage::Tarpaulin) => {
            use crate::tarpaulin::Tarpaulin;
            let tarpaulin_report: sonar::Coverage =
                Tarpaulin::new(&options.tarpaulin_path).try_into()?;
            tarpaulin_report
        }
        None => {
            return Ok(());
        }
    };
    let file = File::create(&options.coverage_path).context(format!(
        "failed to create '{:?}' file",
        &options.coverage_path
    ))?;
    #[cfg(not(debug_assertions))]
    let writer = quick_xml::Writer::new(file);
    #[cfg(debug_assertions)]
    let writer = quick_xml::Writer::new_with_indent(file, b' ', 2);
    let mut serializer = quick_xml::se::Serializer::with_root(writer, Some("coverage"));
    coverage_report.serialize(&mut serializer)?;
    Ok(())
}
