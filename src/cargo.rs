use crate::sonar::TextRange;
use std::{
    collections::BTreeMap,
    fs::File,
    io::{BufRead as _, BufReader},
    path::{Path, PathBuf},
};
use tracing::{error, instrument};

#[derive(Debug)]
pub struct PackageRange {
    pub range: TextRange,
    pub name_range: TextRange,
    pub version_range: TextRange,
}

#[derive(Debug)]
pub struct Lockfile {
    pub lockfile_path: PathBuf,
    pub dependencies: BTreeMap<String, PackageRange>,
}

impl Lockfile {
    pub fn dependency_range(&self, name: &str) -> TextRange {
        self.dependencies.get(name).map_or_else(
            || {
                error!("failed to find a corresponding text range for dependency '{}'. This is probably a bug in `cargo-sonar`, if you can please report an issue at https://gitlab.com/woshilapin/cargo-sonar/-/issues, we'd be happy to try and fix it.", name);
                TextRange {
                    start_line: 1,
                    end_line: 1,
                    start_column: 0,
                    end_column: 1,
                }
            },
            |ranges| ranges.range.clone(),
        )
    }
}

enum StateParser {
    // on '[[package]]'
    Package {
        start_line: usize,
    },
    // on 'name = "package-name"'
    Name {
        start_line: usize,
        package_name: String,
        name_range: TextRange,
    },
    // on 'version = "1.2.3"'
    Version {
        start_line: usize,
        package_name: String,
        name_range: TextRange,
        version_range: TextRange,
    },
    // on empty line
    End,
}

impl TryFrom<&Path> for Lockfile {
    type Error = eyre::Error;

    #[instrument(level = "debug")]
    fn try_from(lockfile_path: &Path) -> Result<Self, Self::Error> {
        let lockfile_path = lockfile_path.canonicalize()?;
        let mut dependencies = BTreeMap::new();
        let file = File::open(&lockfile_path)?;
        let reader = BufReader::new(file);
        let mut state = StateParser::End;
        for (line_num, line) in reader.lines().enumerate() {
            let line_num = line_num.saturating_add(1);
            let line = line?;
            let create_range = |line: &str| {
                let start_column = line.find('"').unwrap_or(0);
                let end_column = line
                    .rfind('"')
                    .unwrap_or_else(|| line.len().saturating_sub(1));
                TextRange {
                    start_line: line_num,
                    end_line: line_num,
                    start_column,
                    end_column,
                }
            };
            match state {
                StateParser::End if line == "[[package]]" => {
                    state = StateParser::Package {
                        start_line: line_num,
                    };
                }
                StateParser::Package { start_line } if line.starts_with("name = ") => {
                    let name_range = create_range(&line);
                    state = StateParser::Name {
                        start_line,
                        package_name: line.replace("name = ", "").replace('"', ""),
                        name_range,
                    };
                }
                StateParser::Name {
                    start_line,
                    package_name,
                    name_range,
                } if line.starts_with("version = ") => {
                    let version_range = create_range(&line);
                    state = StateParser::Version {
                        start_line,
                        package_name,
                        name_range,
                        version_range,
                    };
                }
                StateParser::Version {
                    start_line,
                    package_name,
                    name_range,
                    version_range,
                } if line.is_empty() => {
                    let range = TextRange {
                        start_line,
                        end_line: line_num,
                        start_column: 0,
                        end_column: 0,
                    };
                    let package_range = PackageRange {
                        range,
                        name_range,
                        version_range,
                    };
                    dependencies.insert(package_name, package_range);
                    state = StateParser::End;
                }
                s => state = s,
            }
        }
        let lockfile = Self {
            lockfile_path,
            dependencies,
        };
        Ok(lockfile)
    }
}
